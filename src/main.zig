const std = @import("std");
const vk = @import("vulkan");
const glfw = @import("zglfw");

const GraphicsContext = @import("graphics_context.zig").GraphicsContext;
const Swapchain = @import("swapchain.zig").Swapchain;

const triangle_vert = @embedFile("../shaders/triangle.vert");
const triangle_frag = @embedFile("../shaders/triangle.frag");

pub fn glfwKeyCallback(window: *glfw.Window, keycode: c_int, scancode: c_int, actioncode: c_int, mods: c_int) callconv(.C) void {
        // unused vars
    _ = scancode;
    _ = mods;
    const key = @intToEnum(glfw.Key, keycode);
    const action = @intToEnum(glfw.KeyState, actioncode);
    std.log.info("glfw keyCallback key: {}; action: {}\n", .{key, action});
    if (action != .Press) return;

    switch (key) {
        .Escape => glfw.setWindowShouldClose(window, true),
        .Enter => {},
        .Space => {},
        .Down => {},
        .Left => {},
        .Right => {},
        .Up => {},
        .LeftShift, .RightShift => {},
        .R => {},
        .P => {},
        .LeftControl, .RightControl => {},
        else => {},
    }
}

pub fn main() anyerror!void {
    glfw.init() catch |err| {
        std.log.err("glfw pooped itself! {any}", .{err});
    };
    defer glfw.terminate();

    glfw.windowHint(.ClientAPI, glfw.APIAttribute.NoAPI);


    var extent = vk.Extent2D{ .width = 800, .height = 600 };

    var window = try glfw.createWindow(
        @intCast(i32, extent.width),
        @intCast(i32, extent.height),
        "hello :D",
        null, null
    );
    defer glfw.destroyWindow(window);

    _ = glfw.setKeyCallback(window, glfwKeyCallback);

    const gc = try GraphicsContext.init("Hello :D", window);
    defer gc.deinit();

    std.log.debug("Using device: {s}", .{gc.deviceName()});

    var swapchain = try Swapchain.init(&gc, extent);
    defer swapchain.deinit();

    std.log.debug("vertex shader: {s}", .{triangle_vert});
    std.log.debug("fragment shader: {s}", .{triangle_frag});

    while (!glfw.windowShouldClose(window)) {
        //swapchain.present();

        glfw.pollEvents();
    }
    try swapchain.waitForAllFences();
}
