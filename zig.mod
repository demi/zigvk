id: k0czbi2pjbctvtdym0osoeg2oxvdwtrur6qopm1ek3w9zvwm
name: zigvk
license: MIT
description: test zig+vulkan

dev_dependencies:
    - src: git git@github.com:Snektron/vulkan-zig.git
    - src: git git@github.com:ryndinol/zglfw.git

