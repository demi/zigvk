const std = @import("std");
const deps = @import("./deps.zig");
const vkgen = deps.imports.vulkan_zig;

pub fn build(b: *std.build.Builder) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const exe = b.addExecutable("zigvk", "src/main.zig");
    exe.setTarget(target);
    exe.setBuildMode(mode);

    // Create a step that generates vk.zig (stored in zig-cache) from the provided vulkan registry.
    const vk_xml_path = "/usr/share/vulkan/registry/vk.xml";
    const gen = vkgen.VkGenerateStep.init(b, vk_xml_path, "vk.zig");
    exe.addPackage(gen.package);

    deps.addAllTo(exe);

    exe.install();

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
